package com.techuniversity.prod.controllers;

import com.techuniversity.prod.servicios.ServiciosService;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/servicios")
public class ServicioController {
/*
    @GetMapping("/servicios")
    public List getServicios() {
        return ServiciosService.getAll();
    }*/

    @PostMapping("/servicios")
    public String setServicios(@RequestBody String cadena){
        try {
            ServiciosService.insertBatch(cadena);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }
    @PostMapping("/servicio")
    public String setServicio(@RequestBody String cadena){
        try {
            ServiciosService.insert(cadena);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro){
        return ServiciosService.getFiltrados(filtro);
    }
    @GetMapping("/servicios/periodo")
    public List getServiciosPeriodo(@RequestParam String periodo){
        Document doc = new Document();
        doc.append("disponibilidad.periodos",periodo);
        return ServiciosService.getFiltradosPeriodo(doc);
    }
    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data){
        // En el body del postman pasaremos un objeto con el filtro y los valores
        try {
            JSONObject obj = new JSONObject(data);
            // En el body recibes un objeto con la propiedad filtro y otro con la propiedad valores
            String filtro = obj.getJSONObject("filtro").toString();
            String valores = obj.getJSONObject("valores").toString();
            ServiciosService.update(filtro, valores);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }

}
