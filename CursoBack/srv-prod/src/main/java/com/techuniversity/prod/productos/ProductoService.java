package com.techuniversity.prod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    // Optional es una clase que devuelve un objeto al cual se le puede hacer preguntas, como hay algo?
    // para gestionar la posibilidad de que me devuelva el objeto o no encontrarlo
    public Optional<ProductoModel> findByid(String id){
        return productoRepository.findById(id);
    }

    // .save sirve para hace un post o un put (un insert o un update)
    public ProductoModel save(ProductoModel producto) {
        return productoRepository.save(producto);
    }

    public boolean deleteProducto(ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    //Devuelve una lista de productos paginable

    public List<ProductoModel> findPaginado(int page){
        Pageable pageable = PageRequest.of(page, 3);
        Page<ProductoModel> pages = productoRepository.findAll(pageable);
        List<ProductoModel> productos = pages.getContent();
        return productos;
    }
}
