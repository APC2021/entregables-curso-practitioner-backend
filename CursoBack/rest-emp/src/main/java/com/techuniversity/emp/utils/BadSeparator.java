package com.techuniversity.emp.utils;

public class BadSeparator extends Exception{
    // se sobreescriben los metodos de los que estas heredando
    @Override
    public String getMessage(){
        return "el separador debe ser carácter. No se admite espacio en blanco.";
    }
}
