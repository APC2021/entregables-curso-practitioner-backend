package com.techuniversity.emp.utils;

public class Utilidades {
    public static String getCadena(String texto, String separador) throws BadSeparator{
        if ((separador.length() > 1) || separador.trim().equals(" ")) throw new BadSeparator();
        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";
        for (char letra: letras) {
            if (letra != ' '){
                resultado += letra + separador;
            } else {
                // trim te quita los espacios delante o atras
                if (!resultado.trim().equals("")){
                    // eliminas el ultimo caracter para quitar el punto en caso de que venga espacios
                    resultado = resultado.substring(0, resultado.length() -1);
                }
                resultado += letra;
            }
        }
        // endswith si termina con ...
        if (resultado.endsWith(separador)){
            resultado = resultado.substring(0, resultado.length() -1);
        }
        return resultado;
    }

    public static boolean esImpar(int numero) {
        System.out.println(numero % 2);
        return (numero % 2 != 0);
    }

    public static boolean estaBlanco(String texto){
        return ((texto == null) || texto.trim().isEmpty());
    }

    public static boolean valorarEstadoPedido(EstadosPedido estado){
        // devuelve 0,1,2,3,4
        int valor = estado.ordinal();
        return ((valor >= 0) && (valor <= 5));
    }
}
