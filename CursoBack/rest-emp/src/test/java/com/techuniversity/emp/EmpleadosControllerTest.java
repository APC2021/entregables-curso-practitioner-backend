package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String esperado= "L.U.Z D.E.L S.O.L";
        assertEquals(esperado, empleadosController.getCadena("luz del sol", "."));
    }

    @Test
    public void testBadSeparator() {
        try {
            Utilidades.getCadena("Angel Poveda", "..");
            fail("Se esperaba Bad Separator");
        } catch (BadSeparator bs) {}

    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, -3, -15, Integer.MAX_VALUE})
    public void tesEsImpar(int numero) {
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEstaBlanco (String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testEstaBlancoNull (String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {""," ", "\t", "\n"})   //caracteres que toma como ok
    public void testEstaBlancoNull1 (String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    // se hace tantas veces como estados pedido (enumsource)
    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido estado) {
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }
}
