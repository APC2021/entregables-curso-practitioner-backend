package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class RestempApplicationTests {

	@Test
	void contextLoads() {
	}

	// inyectas el controlador para poder utilizarlo
	@Autowired
	EmpleadosController empleadosController;
	@Test
	public void testHome() {
		// invocas al controlador
		String result = empleadosController.home();
		assertEquals("Don´t worry",result);
	}

}
